require('dotenv').config()
const cors = require('cors');
const express = require('express');
const morgan = require('morgan');
const router = require('./src/routers');
const db = require('./src/config/db');
const app = express()
const port = process.env.PORT

app.use(morgan('combined'));

app.use(express.urlencoded({
  extended: true
}));

app.use(express.json());
app.use(cors())

db.connect();

router(app);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
})