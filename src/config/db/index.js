const mongoose = require('mongoose');

async function connect() {
  // const URI = 'mongodb+srv://admin:TkJGh3c1ZAocYngu@cluster0.6fezz.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
  const URI = 'mongodb://127.0.0.1:27017/task_manager'; // local
  try {
    await mongoose.connect(URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
    });

    console.log('Connect Successfully!!!');
  } catch (e) {
    console.log('Connect Failure!!!');
  }
}

module.exports = {
  connect
}