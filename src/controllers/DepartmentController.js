const Department = require('../models/Departments');
const User = require('../models/Users');
const mongoose = require('mongoose');

class DepartmentController {

  create(req, res) {
    const {name, mission, location, manager} = req.body;

    Department.find({name: name}, function (err, departments) {
      if (err) {
        res.status(401).json({
          message: err.message,
          code: 401
        })
      }

      if (departments.length !== 0) {
        res.status(401).json({
          message: 'Department Name is exist',
          code: 401
        })
      }

      User.findOne({_id: manager}, async function (err, user) {
        if (err) {
          res.json({
            code: 401,
            message: err.message
          })
          return;
        }

        if (!user) {
          res.json({
            code: 401,
            message: 'Manager Name is invalid'
          })
          return;
        }

        const newDepartment = new Department({name, location, mission, user: user._id});

        newDepartment.save().then(result => {
          res.json({
            code: 201,
            data: result
          })
        })
      })
    })
  }

  updateDepartment(req, res) {
    const {department_id, name, manager, location, mission} = req.body;

    Department.findOneAndUpdate({_id: department_id}, {name, location, user: manager, mission}, (err, department) => {
      if (err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      res.json({
        code: 200,
        data: department
      })
    })
  }

  deleteDepartment(req, res) {
    const {department_id} = req.params;

    Department.findOneAndDelete({ _id: department_id}, (err) => {
      if(err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      res.json({
        code: 204,
        message: 'Delete Success!'
      })
    })
  }

  getList(req, res) {
    Department.find({}).populate('user')
      .exec((err, department) => {
        if (err) {
          res.json({
            code: 401,
            message: err.message
          })
          return;
        }

        res.json({
          code: 200,
          data: department
        })
      })
  }
}

module.exports = new DepartmentController;