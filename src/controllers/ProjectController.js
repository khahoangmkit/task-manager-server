const Project = require('../models/Projects');
const Department = require('../models/Departments');

class ProjectController {

  create(req, res) {
    const {department, name, goal, deadline} = req.body;

    Department.findOne({_id: department}, (err, department) => {
      if (err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      if (!department) {
        res.json({
          code: 401,
          message: 'Department name is not exist!'
        })
        return;
      }

      const newProject = new Project({
        name,
        goal,
        deadline,
        department: department._id
      })

      newProject.save().then((result) => {
        Project.populate(newProject, {path: 'project'})
          .then(project => {
            res.json({
              code: 201,
              data: project
            })
          })
      })
    })
  }

  async update(req, res) {
    const {project_id, name, goal, deadline, description, department} = req.body;

    const departmentInfo = await Department.findOne({_id: department});

    if (!departmentInfo) {
      res.json({
        code: 401,
        message: 'Department is not exist'
      })
      return;
    }

    Project.findOneAndUpdate({_id: project_id},
      {name, goal, deadline, description, department: department},
      (err, project) => {
      if (err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      res.json({
        code: 200,
        data: project
      })
    })
  }

  delete(req, res) {
    const {project_id} = req.params;

    Project.findOneAndDelete({_id: project_id}, (err) => {
      if (err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      res.json({
        code: 204,
        message: 'Delete Complete!'
      })
    })
  }

  getList(req, res) {
    Project.find({}).populate('department')
      .exec((err, projects) => {
        if (err) {
          res.json({
            code: 401,
            message: err.message
          })
          return;
        }

        res.json({
          code: 200,
          data: projects
        })
      })
  }

  getProjectById(req, res) {
    const {project_id} = req.params;

    Project.findOne({_id: project_id})
      .populate('department')
      .populate('task')
      .exec((err, project) => {
        if (err) {
          res.json({
            code: 401,
            message: err.message
          })
          return;
        }

        res.json({
          code: 200,
          data: project
        })
      })
  }
}

module.exports = new ProjectController;