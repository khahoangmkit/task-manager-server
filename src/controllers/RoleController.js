const Role = require('../models/Roles');

class RoleController {

  create(req, res) {
    const {name, description} = req.body;


    Role.find({name: name}, function (err, role) {
      if(err) {
        res.json({
          code: 400,
          message: err.message
        })
        return;
      }

      if(role.length !== 0) {
        res.json({
          code: 401,
          message: 'Role is exist.'
        })
        return;
      }

      const newRole = new Role({
        name,
        description
      });

      newRole.save()
        .then((role) => {
          res.json({
            code: 201,
            data: role
          })
        })
        .catch(err => {
          res.json({
            code: 401,
            message: err.message
          })
        })
    })
  }

  update(req, res) {
    const {role_id, name, description} = req.body;

    Role.findOneAndUpdate({_id: role_id}, {name, description}, (err, role) => {
      if (err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      res.json({
        code: 201,
        data: role
      })
    })

  }

  delete(req, res) {
    const {role_id} = req.params;

    Role.findOneAndDelete({_id: role_id}, (err) => {
      if (err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }
      Role.find({},(err, roles) => {
        if (err) {
          res.json({
            code: 401,
            message: err.message
          })
          return;
        }

        res.json({
          code: 200,
          body: {
            data: roles,
            code: 200,
            message: 'Delete Success!'
          }
        })
      })
    })
  }

  getList(req, res) {
    Role.find({}, (err, roles) => {
      if (err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      res.json({
        code: 200,
        data: roles
      })
    })
  }
}

module.exports = new RoleController;