const User = require('../models/Users');
const Project = require('../models/Projects');
const Task = require('../models/Tasks');
const jwt = require("jsonwebtoken");

class TaskController {

  async create(req, res) {
    const data = req.body;

    const {name, description, deadline, goal, users, project} = data;

    Project.findById(project, (err, user) => {
      if(err) {
        res.json({
          code: 401,
          message: 'Department is not exist.'
        })
        return;
      }
    });
    // const userId = await User.findOne({user_name}, {_id: 1});


    users.forEach((userId) => {
      User.findById(userId, (err, user) => {
        if(err) {
          res.json({
            code: 401,
            message: 'User is not exist.'
          })
          return;
        }
      })
    })

    const newTask = new Task({
      name,
      description,
      deadline,
      goal,
      user: users,
      project: project
    })

    newTask.save().then((result) => {
      res.json({
        code: 201,
        data: result
      })
    })
  }

  async update(req, res) {
    const data = req.body;

    const {task_id, name, description, deadline, goal, user_name, project_name} = data;

    const projectId = await Project.findOne({name: project_name}, {_id: 1});
    const userId = await User.findOne({user_name}, {_id: 1});

    Task.findOneAndUpdate({_id: task_id}, {
      name: name,
      description: description,
      deadline: deadline,
      goal: goal,
      user: userId._id,
      project: projectId._id
    }, (err, task) => {
      if (err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      res.json({
        code: 201,
        data: task
      })
    })
  }

  updateProgressTask(req, res) {
    const data = req.body;

    const {_id, done} = data;
    Task.findOneAndUpdate({_id: _id}, {done: done}, (err, task) => {
      if (err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      res.json({
        code: 201,
        data: task
      })
    })
  }

  delete(req, res) {
    const {task_id} = req.params;

    Task.findOneAndDelete({_id: task_id}, (err) => {
      if (err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      res.json({
        code: 401,
        message: 'Delete Success!'
      })
    })
  }

  getTask(req, res) {
    const {task_name} = req.params;

    Task.findOne({name: task_name}, (err, result) => {
      if (err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      res.json({
        code: 200,
        data: result
      })
    })
  }

  getListTask(req, res) {
    const auth = req.headers.authorization;

    const [, token] = auth.split(' ');
    jwt.verify(token, process.env.JWT_KEY, (err, decoded) => {
      if (err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      Task.find({user: decoded.data[0]._id}, (err, tasks) => {
        if (err) {
          res.json({
            code: 401,
            message: err.message
          })
          return
        }

        res.json({
          code: 200,
          data: tasks
        })
      })
    })
  }

  getList(req, res) {
    Task.find({})
      .populate('user')
      .populate('project')
      .exec((err, task) => {
        if (err) {
          res.json({
            code: 401,
            message: err.message
          })
          return;
        }

        res.json({
          code: 200,
          data: task
        })
      })
  }

  getTaskById(req,res) {
    const {task_id} = req.params;
    Task.findById(task_id)
      .populate('project')
      .populate('user')
      .exec((err, task) => {
        if (err) {
          res.json({
            code: 401,
            message: err.message
          })
          return;
        }

        res.json({
          code: 200,
          data: task
        })
      })
  }
}

module.exports = new TaskController;