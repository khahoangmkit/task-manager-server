const md5 = require('md5');
const jwt = require('jsonwebtoken');
const User = require('../models/Users');
const Department = require('../models/Departments');
const Role = require('../models/Roles');


class UserController {

  getList(req, res) {
    User.find({}).populate('department').populate('role')
      .exec((err, users) => {
        if (err) {
          res.json({
            code: 401,
            message: err.message
          })
          return;
        }


        res.json({
          code: 200,
          data: users
        })
      })
  }

  getListUserDepartment(req, res) {
    const {department_id} = req.params;
    User.find({department: {_id: department_id}}, (err, users) => {
      if (err) {
        res.json({
          code: 401,
          error: err.message
        })
        return;
      }

      res.json({
        code: 200,
        data: users
      })
    })
  }

  async register(req, res) {
    const {email, full_name, number_phone, role, department} = req.body;
    const defaultPassword = md5(email);


    const departmentOfUser = await Department.findOne({_id: department});
    const roleOfUser = await Role.findOne({_id: role});

    if (!departmentOfUser) {
      res.json({
        code: 400,
        error: 'Department is not exist.'
      })
      return
    }

    if (!roleOfUser) {
      res.json({
        code: 400,
        error: 'Role is not exist.'
      })
      return
    }

    User.find({user_name: email}, function (err, user) {
      if (err) {
        console.log(err);
        res.status(404).json({
          code: 401,
          error: err.message
        })
        return;
      }

      if (user.length !== 0) {
        res.json({
          code: 401,
          error: 'Username is exist',
        });
        return;
      }

      const infoNewUser = {
        user_name: email,
        email,
        full_name,
        number_phone,
        password: defaultPassword,
        role: roleOfUser._id,
        department: departmentOfUser._id
      }

      const newUser = new User(infoNewUser);

      console.log(newUser);
      newUser.save((err) => {
        if (err) {
          res.status(400).json({
            code: 400,
            message: err.message
          })
        } else {
          res.status(201).json({
            code: 201,
            data: newUser,
            message: 'Create user successful!'
          })
        }
      })
      return;
    });
  }

  remove(req, res) {
    const { user_id } = req.params;
    User.findOneAndDelete({_id: user_id}, (err) => {
      if(err) {
        res.json({
          code: 401,
          message: err.message
        })
        return;
      }

      res.json({
        code: 200,
        message: 'Delete Success!'
      })
    })
  }

  changePassword(req, res) {
    const {user_id, old_password, new_password} = req.body;

    const oldPassword = md5(old_password);
    const newPassword = md5(new_password);

    User.findOneAndUpdate({_id: user_id, password: oldPassword}, {password: newPassword},
      (err, user) => {
        if (err) {
          res.json({
            code: 401,
            error: err.message
          })
          return;
        }

        res.json({
          code: 200,
          data: user
        })
      })

  }

  update(req, res) {
    const {user_id, email, full_name, date_of_birth, number_phone, address, role, department} = req.body;

    User.findOneAndUpdate({_id: user_id},
      {full_name, email, role, address, department, number_phone, date_of_birth},
      (err, user) => {
        if (err) {
          res.json({
            code: 401,
            error: err.message
          })
          return;
        }

        res.json({
          code: 200,
          data: user
        })
      })
  }

  login(req, res) {
    const infoUser = req.body;

    const userName = infoUser.user_name;
    const password = md5(infoUser.password);


    User.find({user_name: userName, password: password}, function (err, user) {
      if (err) {
        res.status(500).json({
          message: err
        })
        return;
      } else if (user.length === 0) {
        res.json({
          code: 401,
          message: 'UserName or Password incorrect'
        })
        return;
      } else {
        const token = jwt.sign({
          data: user
        }, process.env.JWT_KEY, {expiresIn: 60 * 3600});

        res.status(200).json({
          code: 200,
          message: 'success',
          token
        });
        return;
      }
    })
  }

  me(req, res) {
    const auth = req.headers.authorization;

    const [, token] = auth.split(' ');


    jwt.verify(token, process.env.JWT_KEY, async function (err, decoded) {
      if (err) {
        res.status(401).json({
          message: err
        })
        return;
      }

      const role_id = decoded.data[0].role;

      const role = await Role.findOne({_id: role_id});

      console.log(role);
      res.json({
        data: {
          code: 200,
          message: 'success',
          user: decoded.data[0],
          role: role.name
        }
      });
    });
  }

  logout(req, res) {

  }
}

module.exports = new UserController;