const mongoose = require('mongoose');

const {Schema} = mongoose;

const Department = new Schema({
  name: { type: String, maxLength: 255, unique: true},
  mission: {type: String, maxLength: 255},
  location: { type: String, maxLength: 255},
  user: { type: Schema.Types.ObjectId, ref: 'User'}
}, {
  timestamps: true
})

module.exports = mongoose.model('Department', Department);