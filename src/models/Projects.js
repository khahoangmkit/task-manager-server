const mongoose = require('mongoose');

const {Schema} = mongoose;

const Project = new Schema({
  name: { type: String, maxLength: 255, unique: true},
  goal: { type: Number},
  description: {type: String, maxLength: 5000},
  deadline: {type: Date},
  create_time: {type: Date},
  department: { type: Schema.Types.ObjectId, ref: 'Department'},
  task: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Task'
    }
  ]

})

module.exports = mongoose.model('Project', Project);