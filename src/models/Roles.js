const mongoose = require('mongoose');

const {Schema} = mongoose;

const Role = new Schema({
  name: {type: String, maxLength: 255},
  description: {type: String, maxLength: 255},
}, {timestamps: true})

module.exports = mongoose.model('Role', Role);