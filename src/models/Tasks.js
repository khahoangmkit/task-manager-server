const mongoose = require('mongoose');

const {Schema} = mongoose;

const Task = new Schema({
  name: {type: String, maxlength: 255},
  description: {type: String, maxLength: 500},
  deadline: {type: Date},
  done: {type: Number, default: 0},
  goal: {type: Number},
  project: {type: Schema.Types.ObjectId, ref: 'Project'},
  user: [
    {type: Schema.Types.ObjectId, ref: 'User'}
  ]
},{timestamps: true})

module.exports = mongoose.model('Task', Task);