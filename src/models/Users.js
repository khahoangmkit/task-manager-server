const mongoose = require('mongoose');

const {Schema} = mongoose;

const User = new Schema({
  user_name: {type: String, maxlength: 255, unique: true},
  password: {type: String, minLength: 8},
  full_name: {type: String, maxlength: 255},
  date_of_birth: {type: Date, default: null},
  address: {type: String, maxlength: 255, default: null},
  number_phone: {type: String, maxlength: 255, default: null},
  email: {type: String, maxlength: 255},
  department: {type: Schema.Types.ObjectId, ref: 'Department'},
  role: {type: Schema.Types.ObjectId, ref: 'Role'}
}, {
  timestamps: true
});

module.exports = mongoose.model('User', User);