const express = require('express');
const router = express.Router();

const departmentController = require('../controllers/DepartmentController');

router.get('/list', departmentController.getList);
router.post('/update', departmentController.updateDepartment);
router.delete('/delete/:department_id', departmentController.deleteDepartment);
router.post('/create', departmentController.create);

module.exports = router;
