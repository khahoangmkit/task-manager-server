const siteRoute = require('./site');
const userRoute = require('./user');
const departmentRouter = require('./department');
const projectRouter = require('./project');
const taskRouter = require('./task');
const roleRouter = require('./role');

function router(app) {

  app.use('/user', userRoute);
  app.use('/department', departmentRouter);
  app.use('/project', projectRouter);
  app.use('/task', taskRouter);
  app.use('/role', roleRouter);

  app.use('/', siteRoute);

}

module.exports = router;