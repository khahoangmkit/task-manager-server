const express = require('express');
const router = express.Router();

const projectController = require('../controllers/ProjectController');

router.get('/list', projectController.getList);
router.post('/create', projectController.create);
router.post('/update', projectController.update);
router.delete('/delete/:project_id', projectController.delete);
router.get('/:project_id', projectController.getProjectById);

module.exports = router;
