const express = require('express');
const router = express.Router();

const roleController = require('../controllers/RoleController');

router.post('/create', roleController.create);
router.get('/list', roleController.getList);
router.post('/update', roleController.update);
router.delete('/delete/:role_id', roleController.delete);

module.exports = router;
