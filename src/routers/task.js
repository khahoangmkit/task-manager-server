const express = require('express');
const router = express.Router();

const taskController = require('../controllers/TaskController');

router.post('/create', taskController.create);
router.get('/list', taskController.getList);
router.get('/:task_id', taskController.getTaskById);
router.post('/update/progress', taskController.updateProgressTask);
router.post('/update', taskController.update);
router.delete('/delete/:task_id', taskController.delete);

module.exports = router;
