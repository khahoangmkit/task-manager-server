const express = require('express');
const router = express.Router();

const userController = require('../controllers/UserController');

router.get('/list/:department_id', userController.getListUserDepartment);
router.get('/list', userController.getList);
router.post('/update/password', userController.changePassword);
router.post('/update', userController.update);
router.post('/register', userController.register);
router.delete('/delete/:user_id', userController.remove);
router.post('/login', userController.login);
router.get('/me', userController.me);


module.exports = router;
